<?php
//use App\Mail\WelcomeMail;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Route::get('/email', function () {
//    return new WelcomeMail();
//});


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::resource('/user', 'userController');
Route::resource('/post', 'PostController');
Route::get('/comment/{id}', 'PostController@comment')->name('comment');
Route::post('/likes/{id}', 'PostController@likes')->name('likes');
Route::post('/filter/{id}', 'PostController@filter')->name('filter');
Route::get('/filter/{id}', 'PostController@filter')->name('filter');
Route::get('/imgPostType', 'PostController@imgPostType')->name('imgPostType');
Route::get('/videoPostType', 'PostController@videoPostType')->name('videoPostType');
