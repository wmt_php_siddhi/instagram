@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-5">



{{--                <div class="form-group row">--}}
{{--                    <label for="country_name" class="col-md-4 col-form-label text-md-right">Country</label>--}}
{{--                    <select class="custom-select col-md-4 ml-3 col-form-label text-md-right @error('') is-invalid @enderror" name="country_name" id="country_name">--}}
{{--                        <option hidden value="">Country</option>--}}
{{--                        @foreach($countries as $countries)--}}

{{--                            <form method="POST" action="{{url("/filter/{$countries->id}")}}" enctype="multipart/form-data">--}}
{{--                                @csrf--}}
{{--                                @method("get")--}}
{{--                                <option value="{{ $countries->id }}">{{ $countries->country_name }}</option>--}}
{{--                            </form>--}}
{{--                        @endforeach--}}
{{--                    </select>--}}
{{--                </div>--}}



                <div class="dropdown">
                    <button class="btn btn-secondary dropdown-toggle form-control" type="button"
                            id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
                            aria-expanded="false">
                        Country
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        @foreach($countries as $countries)
                            <form method="post" class="form" action="{{url("/filter/{$countries->id}")}}">
                                @csrf
                                @method('get')
                                <button class="btn btn-outline-primary form-control" type="submit"
                                        value="{{ $countries->id }}">{{ $countries->country_name }}</button>
                            </form>
                        @endforeach
                    </div>
                </div>

                <div class="dropdown">

                    <form method="post" class="form" action="{{route('imgPostType')}}">
                        @csrf
                        @method('get')
                        <button class="btn btn-outline-primary form-control" type="submit">Images</button>
                    </form>
                </div>

                <div class="dropdown">

                    <form method="post" class="form" action="{{route('videoPostType')}}">
                        @csrf
                        @method('get')
                        <button class="btn btn-outline-primary form-control" type="submit">video</button>
                    </form>
                </div>


            @foreach($data as $data)
                    <div class="card-body">
                <div class="card align-items-center">






                    <img src="{{url('/storage/'.$data->post_url)}}" width="100% ;" height="400px;">



                    <form method="Post" action="{{url("comment/{$data->id}")}}">
                        @csrf
                        @method('get')
                        @if(Auth::id()!= $data->user_id)
                        <input type="text" name="comment"  placeholder="comment..." class="mt-2"><br>
                        <input type="submit" class="btn btn-primary mt-2" value="Comment">

                        @endif
                    </form>


                         @foreach($data->comments as $comment)
                        <b><label>comment by :{{$comment->user->name}}</label></b>
                        {{$comment->comments}}

                         @endforeach
                    @if($data->likes == 1)
                        <form action="{{ route('likes', $data->id) }}" method="POST">
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-primary" name="likes" value="1">like</button>
                        </form>
                    @else
                        <form action="{{ route('likes', $data->id) }}" method="POST">
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-secondary" name="likes" value="0">dislike</button>
                        </form>
                    @endif

                        @if(Auth::id() == $data->user_id)
                        <form method="post" class="delete_form" action="/post/{{$data->id}}">
                            @csrf
                            <input type="hidden" name="_method" value="DELETE" />
                            <button type="submit" class="btn btn-danger brn-large">Delete</button>
                        </form>
                    @endif

                </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection
