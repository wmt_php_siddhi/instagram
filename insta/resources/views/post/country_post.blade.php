@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-5">
        @foreach($filter as $filter)
            <div class="card-body">
                <div class="card align-items-center">

                    <img src="{{url('/storage/'.$filter->post_url)}}" width="100% ;" height="400px;">

                    <form method="Post" action="{{url("comment/{$filter->id}")}}">
                        @csrf
                        @method('get')
                        @if(Auth::id()!= $filter->user_id)
                        <input type="text" name="comment"  placeholder="comment..." class="mt-2"><br>
                        <input type="submit" class="btn btn-primary mt-2" value="Comment">
                        @endif
                    </form>


                        @foreach($filter->comments as $comment)

                            <b><label>comment by :{{$comment->user->name}}</label></b>
                            {{$comment->comments}}

                        @endforeach

                    @if($filter->likes == 1)
                        <form action="{{ route('likes', $filter->id) }}" method="POST">
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-secondary" name="likes" value="0">dislike</button>
                        </form>
                    @else
                        <form action="{{ route('likes', $filter->id) }}" method="POST">
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-primary" name="likes" value="1">like</button>
                        </form>
                    @endif

                    @if(Auth::id() == $filter->user_id)
                        <form method="post" class="delete_form" action="/filter/{{$filter->id}}">
                            @csrf
                            <input type="hidden" name="_method" value="DELETE" />
                            <button type="submit" class="btn btn-danger brn-large">Delete</button>
                        </form>
                    @endif


                </div>
            </div>
            @endforeach
            </div>
        </div>
    </div>
@endsection

