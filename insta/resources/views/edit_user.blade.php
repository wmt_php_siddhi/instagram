@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Edit Profile') }}</div>

                    <div class="card-body">
                        {{--                        <form method="POST" action="{{ route('register') }}" enctype="multipart/form-data">--}}
                        <form action={{route('user.update',$data->id)}} method="post" enctype="multipart/form-data">
                            {{--        <input type="hidden" name="_token" value="{{csrf_token()}}">--}}
                            {{csrf_field()}}
                            @method('PATCH')
                            <div class="form-group row">
                                <label for="country_name" class="col-md-4 col-form-label text-md-right">Country</label>
                                <select class="custom-select col-md-4 ml-3 col-form-label text-md-right @error('') is-invalid @enderror"  name="country_name" id="country_name">
                                    <option hidden>Country</option>
                                    @foreach($countries as $country)
                                        <option value="{{ $country->id }}" {{ $data->country_id==$country->id?'selected':'' }}>{{ $country->country_name }}</option>
                                    @endforeach
                                </select>
                                @error('country_name')
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                                 </span>
                                @enderror
                            </div>
                            <div class="form-group row">
                                <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{$data->name}}" autocomplete="name" >

                                    @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="username" class="col-md-4 col-form-label text-md-right">{{ __('UserName') }}</label>

                                <div class="col-md-6">
                                    <input id="username" type="text" class="form-control @error('username') is-invalid @enderror" name="username" value="{{$data->username}}"  autocomplete="username" >

                                    @error('username')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                                <div class="col-md-6">
                                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{$data->email}}"  autocomplete="email">

                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="dob" class="col-md-4 col-form-label text-md-right">{{ __('Date of Birth') }}</label>

                                <div class="col-md-6">
                                    <input id="dob" type="date" class="form-control @error('dob') is-invalid @enderror" name="dob" value="{{$data->dob}}"  autocomplete="dob" >

                                    @error('dob')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                @if ("/storage/{{ $data->images }}")
                                    <img src="{{ $data->image }}">
                                @else
                                    <p>No image found</p>
                                @endif
                                <label for="profile_pic" class="col-md-4 col-form-label text-md-right">{{ __('profile_pic') }}</label>

                                <div class="col-md-6">
                                    <input id="profile_pic" type="file" class="form-control @error('profile_pic') is-invalid @enderror" name="profile_pic" value="{{ $data->profile_pic}}"  autocomplete="profile_pic">

                                    @error('profile_pic')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('update') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

