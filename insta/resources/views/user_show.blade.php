@extends('layouts.app')
<meta charset="UTF-8">
<meta name="viewport"
      content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">

<title>Show</title>
@section('content')
    <div class="container">

{{--        <table class="table">--}}
{{--            <thead>--}}
{{--            <tr>--}}
{{--                <th scope="col">Country</th>--}}
{{--                <th scope="col">Name</th>--}}
{{--                <th scope="col">UserName</th>--}}
{{--                <th scope="col">Email</th>--}}
{{--                <th scope="col">Date_of_birth</th>--}}
{{--                <th scope="col">profile_pic</th>--}}
{{--                <th scope="col">Edit</th>--}}
{{--                <th scope="col">Delete</th>--}}
{{--            </tr>--}}
{{--            </thead>--}}
{{--            <tbody>--}}

{{--                <tr>--}}

{{--                    <td>{{$data->country_id}}</td>--}}
{{--                    <td>{{$data->name}}</td>--}}
{{--                    <td>{{$data->username}}</td>--}}
{{--                    <td>{{$data->email}}</td>--}}
{{--                    <td>{{$data->dob}}</td>--}}
{{--                    <td><img src="{{url('/storage/'.$data->profile_pic)}}" width="100px;" height="100px;"></td>--}}
{{--                    <td><a type="button" class="btn btn-success" href="/user/{{$data->id}}/edit">Edit</a></td>--}}
{{--                    <td>--}}
{{--                        <form method="post" class="delete_form" action="/user/{{$data->id}}">--}}
{{--                            @csrf--}}
{{--                            <input type="hidden" name="_method" value="DELETE" />--}}
{{--                            <button type="submit" class="btn btn-danger">Delete</button>--}}
{{--                        </form>--}}
{{--                    </td>--}}

{{--                </tr>--}}

{{--            </tbody>--}}
{{--        </table>--}}


        <div class="card border-dark col-md-8 offset-md-4" style="width:300px">
            <div class="card-header">My Profile</div>
                 <div class="card-body">
                <p><strong>Profile pic :</strong><img src="{{url('/storage/'.$data->profile_pic)}}" class="mt-3 ml-3" width="150px;" height="150px;"></p>
                <p><strong class="mr-5">Country:</strong> {{$data->country->country_name}}</p>
                <p><strong class="mr-5">Name :</strong> {{$data->name}}</p>
                <p><strong class="mr-3">UserName :</strong>{{$data->username}}</p>
                <p><strong class="mr-4">Email :</strong>{{$data->email}}</p>
                <p><strong class="mr-3">Date_of_Birth :</strong>{{$data->dob}}</p>
                <a type="button" class="btn btn-success" href="/user/{{$data->id}}/edit">Edit</a>
                </div>

    </div>
        </div>
@endsection
