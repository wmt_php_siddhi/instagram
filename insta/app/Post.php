<?php

namespace App;

use App\Comment;
use App\User;
use Illuminate\Database\Eloquent\Model;


class   Post extends Model
{
    public function users()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function usr()
    {
        return $this->hasMany(User::class);
    }

    public function getCoverImgAttributes($value)
    {

        return url('/storage/' . $value);
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }
//    public function usr(){
//        return $this->belongsToMany(User::class);
//    }
    protected $fillable = [
        'user_id', 'title', 'post_url','post_type'
    ];
}
