<?php

namespace App;

use App\User;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    public function country()
    {
        return $this->hasMany('User::class');
    }

    public function posts()
    {
        return $this->hasManyThrough(Post::class, User::class, 'country_id', 'user_id', 'id', 'id');
    }
}
