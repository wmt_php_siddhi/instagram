<?php

namespace App\Http\Controllers;

use App\Comment;
use App\User;
use App\Post;
use App\Country;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
//        if (\Auth::check()) {
            $data = Post::orderBy('created_at', 'desc')->get();

            $countries = Country::all();
//          dd($countries);
//            return view('post.post', compact('data','countries'));
            return view('post.post')->with('data', $data)
                ->with('countries', $countries);


//        } else {
//            return redirect('login');
//        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {


        return view('post.add_post');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => ['required', 'string'],
            'post_url' => ['required', 'max:50000', 'mimes:jpeg,jpg,png,bmp,mpv4,mov,ogg,gif'],

        ]);
        $image_type = ['jpeg','png','jpg','gif'];
        $image = $request->file('post_url');
        $originalname = uniqid('Img', 10) . '.' . $image->getClientOriginalExtension();
//        $path = in_array($originalname,$image_type)->storeAs('/post_url', $originalname, 'public');
      $path = $image->storeAs('/post_url', $originalname, 'public');
       $data = Post::create([

            'user_id' => Auth::id(),
            'title' => $request->title,
            'post_url' => $path,
            'post_type'=> in_array($originalname,$image_type) ? 'image' : 'video'
        ]);


//        $data = new Post();
//        $data->user_id = Auth::id();
//        $data->title = $request->title;
//        $image = $request->file('post_url');
//        $originalname = uniqid('Img',10) .'.'.$image->getClientOriginalExtension();
//        $path = $image->storeAs('/userimagepost',$originalname,'public');
//        $data->post_url = $path;
//        $data->save();
        return redirect()->route('post.index');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Post::find($id);
        $data->delete();
        return redirect()->route('post.index')->with('success', 'Data Deleted');
    }

    public function comment(Request $request, $id)
    {
//        $data = new Comment();
//        $data->user_id = Auth::id();
//        $data->post_id = $id;
//        $data->comments = $request->comment;
//        $data->save();
//        return redirect()->route('post.index');

        $data = Comment::create([
            'user_id' => Auth::id(),
            'post_id' => $id,
            'comments' => $request->comment

        ]);
//        $data->save();
        return redirect()->route('post.index');

    }

    public function likes($id)
    {
        $data = Post::find($id);
        if ($data->likes == 1) {
            $data->likes = 0;
        } else {
            $data->likes = 1;
        }
        $data->save();
        return redirect()->route('post.index');
    }

    public function filter($id)
    {
        $filter = Country::find($id)->posts;
        return view('post.country_post', compact('filter'));

    }
    public function imgPostType(){

        $postType = Post::all()->where('post_type' ,'=', 'image');

            return view('post.imagepost', compact('postType'));

//        else{
//            return view('post.videopost',compact('postType'));
//        }

//        return redirect()->route('post.index');
    }
    public function videoPostType()
    {

        $postType = Post::all()->where('post_type', '=', 'video');

        return view('post.videopost', compact('postType'));
    }
}
