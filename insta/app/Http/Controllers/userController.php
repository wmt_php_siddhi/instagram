<?php

namespace App\Http\Controllers;

use App\Authors;
use App\Country;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class userController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

//        if (\Auth::check()) {

            $data = Auth::user();

            return view('user_show')->with('data', $data);;
//        } else {
//            return redirect('login');
//        }
//        return view('home');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = User::find($id);
        $countries = Country::all();
        return view('edit_user ', compact('data', 'countries'));
    }

    /**
     * Update the specified resource in storage.
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = User::find($id);

        $request->validate([
            'name' => ['required', 'alpha_dash'],
            'username' => ['required'],
            'email' => ['required', 'unique:users,email,' . $data->id],
            'dob' => ['required', 'string'],
            'profile_pic' => ['mimes:jpeg,jpg,png','unique:users,profile_pic,' . $data->id],
        ]);

        $data->country_id = $request->country_name;
        $data->name = $request->name;
        $data->username = $request->username;
        $data->email = $request->email;
        $data->dob = $request->dob;


        if ($request->profile_pic) {

            $image = $request->file('profile_pic');
            $originalname = uniqid('Img', 10) . '.' . $image->getClientOriginalExtension();
            $path = $image->storeAs('/profile_pic', $originalname, 'public');
            $data->profile_pic = $path;
        }
        $data->save();
        return redirect()->route('user.index');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = User::find($id);
        $data->delete();
        return redirect()->route('user.index')->with('success', 'Data Deleted');
    }
}
